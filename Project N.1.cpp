#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Proyecto N.1 de Metodos numericos
int input,inputm2, iter, factorial, o, i, counter, returnkey, a, cdcounter = 0;
float c , real, termino, deva, termap, errep, errabs, aproxvieja, errea, A[20] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

int main(int argc, char** argv) {
	do
	{
		system("@cls||clear");
		printf("Este programa permite encontrar el valor aproximado de una funcion (exponencial, logaritmica o polinomial) aplicando la Serie de Taylor (Mclaurin).\n\n");
		printf("               Menu\n          Teoria del error\n");
		printf("1. Hoja de Presentacion\n");
		printf("2. Calculo del error por Truncamiento\n   (Serie de Taylor)\n");
		printf("3. Salir del programa\n\n");
		printf("Seleccione la opcion que desea revisar: ");


		do
		{
			counter = 0;
			fflush(stdin);
			scanf("%d",&input);
			if (input > 3)
			{
				printf("por favor introduzca una opcion valida: ");
			}
			else
			{
				if (input < 1)
				{
					printf("por favor introduzca una opcion valida: ");
				}
				else
				{
					counter = 1;
				}
			}
		} while (counter != 1);

    	system("@cls||clear");

		//menu principal

		switch (input)
		{

			//Presentacion
			case (1):
				printf("\nEste programa es obra de Adrian Bacceli // E-8-147862\n\nPara volver al menu principal presione cualquier tecla\n");
				system("pause");
				system("@cls||clear");
				break;


			// menu N.2
			case (2):

				/*limpia las variables que no estan en uso*/
				iter = 0;
				factorial = 0;
				o = 0;			/*loop factorial*/
				i = 0;			/*iteraciones*/
				c = 0;			/*Valor de X*/
				real = 0;		/*funcion real*/
				termino = 0;	/*valor en loop i*/
				deva = 0;		/*valor fijo derivada*/
				termap = 0;		/*valor termino aprox*/
				errep = 0;		/*valor error relativo porcentual*/
				errabs = 0;		/*valor error absoluto*/
				aproxvieja = 0;	/*Aproximacion vieja error relativo aproximado*/
				errea = 0;		/*error relativo aproximado*/
				returnkey = 0;	/*condicion de retorno al menu 2*/
				
				for (a=1;a<(20);a++)		/*Loop para limpiar array A*/
					{
						
						A[a] = 0;
						
					}
				
				fflush(stdin);

				/*introduce un valor para c (x)*/
				printf("Indica el valor a evaluar para X: ");
				
				
				/*revisa que el valor introducido quepa en la memoria*/
				do
				{
						cdcounter = 0;
						fflush(stdin);
						scanf("%f",&c);
						fflush(stdin);
						if (c > 3.3)
						{
							printf("no hay suficiente memoria para calcular ese valor, la funcion tiende a infinito.\nPor favor introduzca un numero menor, no mayor a 3.3: ");
						}
						else
						{
							if (c < 0)
							{
								printf("por favor introduzca un numero mayor a 0: ");
							}
							else
							{
								cdcounter = 1;
							}
						}
				} while (cdcounter != 1);

				/*calcula el valor real*/				
				real=((exp(c))+(2*c)+(1));

				/*se abre ciclo del menu 2*/
				do
				{	/*menu 2 */
					system("@cls||clear");
					// Se usa para visualizar los valores almacenados en el programa
					
					/* printf("input: %d inputm2: %d iter: %d factorial: %f o: %d n: %d i: %d ccounter: %d counter: %d cdcounter: %d c: %f real: %f termino: %f deva: 
					%f termap: %f errep: %f errabs: %f aproxvieja: %f errea: %f\n\n",input,inputm2, iter, factorial, o,i, ccounter, counter, cdcounter,c,real,
					termino,deva,termap,errep,errabs,aproxvieja,errea);*/
					
					//system("pause");
					printf("             Menu 2\n     Error por Truncamiento\n         Serie de Taylor\n\n");
					printf("1. Tabla de Resultados\n");
					printf("2. Error cometido en el n-esimo termino\n");
					printf("3. Valor aproximado de la funcion\n");
					printf("4. Valor exacto de la funcion\n");
					printf("5. Regresar al Menu 1\n\n");
					printf("Seleccione la opcion que desea revisar: ");

					do		/*Validaddor de opciones en el menu de 1 a 5*/
					{		
							counter = 0;
							fflush(stdin);
							scanf("%d",&inputm2);
							if (inputm2 > 5)
							{
								printf("por favor introduzca una opcion valida: ");
							}
							else
							{
								if (inputm2 < 1)
								{
									printf("por favor introduzca una opcion valida: ");
								}
								else
								{
									counter = 1;
								}
							}
					} while (counter != 1);

					/*Opciones del menu*/
					switch (inputm2)
					{
						/*opcion 1*/
						case (1):
							
							// Limpieza de variables - para eventos de retorno al menu 2 caso 1
							iter = 0;
							factorial = 0;
							o = 0;			/*loop factorial*/
							i = 0;			/*iteraciones*/
							//c = 0;		/*Valor de X, ya se necesita definido*/
							//real = 0;		/*funcion real, ya se necesita definida*/
							termino = 0;	/*valor en loop i*/
							deva = 0;		/*valor fijo derivada*/
							termap = 0;		/*valor termino aprox*/
							errep = 0;		/*valor error relativo porcentual*/
							errabs = 0;		/*valor error absoluto*/
							aproxvieja = 0;	/*Aproximacion vieja error relativo aproximado*/
							errea = 0;		/*error relativo aproximado*/
							returnkey = 0;	/*condicion de retorno al menu 2*/
							fflush(stdin);
							
							//Inicio de menu 2 - Opcion 1 - Tabla de valores
							system("@cls||clear");
							printf("\nUsted ha seleccionado la opcion de Tabla de resultados\nA continuacion presentamos la tabla: \n");
							printf("Iteracion   aproximacion  Error absoluto Error relativo  Error relativo \n                                           porcentual     aproximado\n\n");

							/*Define el margen de error para las iteraciones*/

							errea = 1;
							for (i = 1; errea > 0.0001; i++)
							{
								if (i<2)		/*define el valor de la derivada para la iteracion 1*/
								{
									deva = 2;
								}
								else
								{
									if (i<3)	/*define el valor de la derivada para la iteracion 2*/
									{
										deva = 3;
									}
									else		/*define el valor de la derivada para la iteracion 3*/
									{
										deva = 1;
									}
								}
								
								/*Calculo de los resultados tabulares e imprime*/

								if (i<2)	/*Primera iteracion*/
								{
									termino = deva * pow(c,(0));
									termap = termap + termino;
									errabs= abs(real-termap);
									errep=errabs/real*100;
									// Operacion del calculo para el termino y su aproximacion
									//printf("primera iteracion \nderiv. Ev * (x ^ 0) / 1 = ");
									//printf("%f * (%f ^ %d) / %d  =  %f       %f \n",deva,c,i-1,factorial,termino,termap);

									printf("    %02d       %09.6f       %09.6f     %%%09.6f    ------------\n",(i-1),termap,errabs,errep);
								}
								else	/*2da iteracion en adelante*/
								{
									factorial=1;
									for (o=1;o<=(i-1);o++)
									{
										factorial*=o;
									}
									aproxvieja=termap;
									termino = pow((deva*c),(i-1))/(factorial);
									termap = termap + termino;
									errabs= abs(real-termap);
									errep=errabs/real*100;
									errea=(termap-aproxvieja)/termap;
									// Operacion del calculo para el termino y su aproximacion
									//printf("deriv. Ev * (x ^ iter) / factorial = ");
									//printf("%f * (%f ^ %d) / %d  =  %f       %f \n",deva,c,i-1,factorial,termino,termap);
									printf("    %02d       %09.6f       %09.6f     %%%09.6f      %f\n",(i-1),termap,errabs,errep,errea);
								}
								A[i] = termino;
							}
							returnkey=0;
							printf("\n");
							system("pause");
							break;

						/*Opcion 2*/
						case (2):
							system("@cls||clear");
							printf("\nUsted ha seleccionado la opcion de Error cometido en el n-esimo termino\n");
							printf("Por favor indique el termino que desea buscar: ");
							fflush(stdin);
							scanf("%d",&a);
							fflush(stdin);
							//for (a=1;a<(20);a++)		/*Loop para verificar array A*/
							//		{
										if (A[a] != 0)	/*imprime valor evaluado en la funcion*/
										{
											printf("\nEl valor %d en la memoria es %f\n\n",a,A[a+1]);
										}
										else
										{
											printf("Esa iteracion no es valida\n");
										}
							//		}
							system("pause");
							returnkey=0;
							break;

						/*Opcion 3*/
						case (3):
							system("@cls||clear");
							printf("\nValor aproximado de la funcion\n\nEl valor aproximado de la funcion evaluada es %f \n\n",termap);
							system("pause");
							returnkey=0;
							break;

						/*Opcion 4*/
						case (4):
							system("@cls||clear");
							printf("\nValor exacto de la funcion\n\nEl valor exacto de la funcion evaluada es %f \n\n",real);
							system("pause");
							returnkey=0;
							break;

						/*Opcion 5*/
						case (5):
							returnkey=1;
							break;

					}

				} while (returnkey==0);

				break;
		}

	} while (input != 3);

	printf("El programa ha finalizado sin errores\n");
	system("pause");
	return 0;
}