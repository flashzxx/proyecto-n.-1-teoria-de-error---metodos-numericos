# Proyecto N.1 Métodos Numéricos - Adrián Bacceli
Este repositorio comprende el Proyecto N.1 de Métodos Numéricos - Universidad Tecnológica de Panamá.

El programa permite realizar el cálculo del valor aproximado de la función exponencial e^x+2x+1 mediante la aplicación iterativa del método de MacLaurin.

El método de MacLaurin consiste en la aplicación del modelo matemático de Taylor (Serie de Taylor) en el cual, se realiza la sumatoria de la funcion evaluada en x0 + su 1ra derivada evaluada en x0 / 1! * x + su 2da derivada evaluada en x0 / 2! * x² + su 3ra derivada evaluada en x0 / 3! * x³ + ... + su n-ava derivada evaluada en x0 / n! * xⁿ 
donde X0 adopta el valor de 0 para toda la serie.

Método de taylor:

    f(x) = f(x0) + [f'(x0)/1!]*[x-x0] + [f''(x0)/2!]*[x-x0]² + [f'''(x0)/3!]*[x-x0]³ ... + [fⁿ'(x0)/n!]*[x-x0]ⁿ


Método de Maclaurin (donde x0 vale 0):

    f(x) = f(0) + [f'(0)/1!]*[x] + [f''(0)/2!]*[x]² + [f'''(0)/3!]*[x]³ ... + [fⁿ'(0)/n!]*[x]ⁿ


El programa cuenta con las siguientes funciones:

    -Tiene definido los valores de las derivadas.
    -Cuenta con un espacio de memoria de valores float (8 bytes).
    -Permite el cálculo (limitado por la memoria) hasta x = 3.3 (el valor exponencial tiende a infinito, los números superan los 8 bytes).
    -El usuario introduce el valor de X.
    -Tendrá un sub-menú con 4 opciones. 
 
        1- Gráfica iterativa tabular de los cálculos de aproximación. 
        2- Valor de aproximación en el n-ésimo término.
        3- Valor aproximado (cuando el porcentaje de error absoluto tiende a < 0.01%).
        4- Valor real de la expresión

        Nota: es necesario realizar el cálculo de la gráfica iterativa tabular (opción 1) antes de proceder con los pasos 2 y 3.

Revisar el archivo de changelog para más detalles.

Autor: Adrián Bacceli  -  Todos los derechos reservados.